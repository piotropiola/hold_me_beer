import React from 'react';
import {ListItem} from './listItem';
import {setLocalStorage} from '../../globals/globals';

class List extends React.Component {

  constructor(props) {
    super(props);
      this.state = {
        selected: '',
        limitVal:5,
        limit:5,
        beers:[],
        selectedBeers:[]
      };

    this.changeSelect = this.changeSelect.bind(this);
    this.selectBeers = this.selectBeers.bind(this);
    this.onLoadMore = this.onLoadMore.bind(this);
    this.renderLoadMore = this.renderLoadMore.bind(this);
    this.sortBy = this.sortBy.bind(this);
    this.selectBeers = this.selectBeers.bind(this);

  }

  componentDidMount(){
    let local ='';
    local = localStorage.getItem('list'+this.props.id);
    local = JSON.parse(local);
    this.setState({
      beers: this.props.beerList,
      limitVal: this.props.limit,
      limit: this.props.limit,
      selected:local
    }, () => { 
        this.selectBeers(this.state.selected);
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      limitVal: nextProps.limit,
      limit: nextProps.limit
    });
    if(this.state.selectedBeers.length > 0){
      this.sortBy(this.state.selectedBeers,nextProps.orderBy,nextProps.order)
    }
  }

  changeSelect(event) {
    setLocalStorage('list'+this.props.id,event.target.value);
    this.setState({
      selected: event.target.value
    },this.selectBeers(event.target.value));
  }


  selectBeers(selectedBrewer){
    const beers = this.state.beers;
    let selectBeers = [];
    const result = Object.values(beers);
        result.map(function(key, index){
          if(key.brewer == selectedBrewer ){
            selectBeers.push(key);
          }
        })
    selectBeers = this.sortBy(selectBeers,this.props.orderBy,this.props.order);
    this.setState({
      selectedBeers: selectBeers
    })
  }

  onLoadMore(event) {
      event.preventDefault();
      this.setState({
          limit: this.state.limit + this.state.limitVal
      });
  }

  renderBeers(){
      return this.state.selectedBeers.slice(0,this.state.limit).map((item,i)=>{
          return(
            <ListItem
              key={i}
              beerInfo={item}
            />
          );
      });
  }

  sortBy(value,key,direction){
    let newSelected;
    if(key == 'price'){
        newSelected = value.sort( (a,b)=>(
          direction === 'ASC'
            ? parseFloat(a[key]) - parseFloat(b[key])
            : parseFloat(b[key]) - parseFloat(a[key])
        ))
    }else{
        newSelected = value.sort(function(a, b){
            var nameA=a[key].toLowerCase(), nameB=b[key].toLowerCase()
            if(direction == "ASC"){
              if (nameA < nameB)
                  return -1 
              if (nameA > nameB)
                  return 1
            }else{
              if (nameB < nameA)
                  return -1 
              if (nameB > nameA)
                  return 1
            }
            return 0 //default return value (no sorting)
        })
    }
    return newSelected;
  }

  renderLoadMore(){
    if(this.state.selectedBeers.length > this.state.limit){
      return(
        <a href="#" className='listWrap__more' onClick={this.onLoadMore}>Load</a>
      );
    }else{
      return 
    }
  }

  render() {
    return (
        <div className="listWrap">
          <select className="listWrap__select" defaultValue={'DEFAULT'} onChange={this.changeSelect}>
            <option value="DEFAULT" disabled>Choose Brewer</option>
            {this.props.selectOptions.map((selectOption) =>
              <option value={selectOption} key={selectOption}>{selectOption}</option>
            )}
          </select>
          {this.state.selectedBeers.length > 0  ?  
            this.renderBeers()
          : (
            <p className="listWrap__empty">Brewer not selected </p>
          )}
          {this.renderLoadMore()}
        </div>
    );
  }
}

export {List}