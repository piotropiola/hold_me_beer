import React from 'react';
import {ImagePopup} from './imagePopup';

class ListItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      openPopup: false
    }

    this.openPopup = this.openPopup.bind(this);
    this.closePopup = this.closePopup.bind(this);
  }

  openPopup(){
    this.setState({
      openPopup: true
    })
  }

  closePopup(){
    this.setState({
      openPopup: false
    })
  }



  render() {
    const beer = this.props.beerInfo;

    return (
      <div className='listWrap__element'>
          <img 
            onError={(e)=>{e.target.onerror = null; e.target.src="/favicon.ico"}}
            src={beer.thumbnail} 
            alt={beer.name}  
            className="listWrap__element__image"
            onClick={this.openPopup}
          />
          <ImagePopup
            img={beer.thumbnail}
            alt={beer.name}
            open={this.state.openPopup}
            close={this.closePopup}
          />

          <div className='listWrap__element__desc'>
            <h2>{beer.name}</h2>
            <p>Type: {beer.type}</p>
            <p>Price/Litre: {beer.price}</p>
          </div>
      </div>
    );
  }
}

export {ListItem}