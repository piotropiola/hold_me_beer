import React from 'react';

class ImagePopup extends React.Component {

  constructor(props) {
    super(props);
  }




  render() {
    let open = this.props.open;
    return (      

      <div className={'imagePopup ' + (open === true ? 'imagePopup--visible' : '')}>
        <div className="imagePopup__wrap">
          <span className="imagePopup__close" onClick={this.props.close}>X</span>
          <img 
            src={this.props.img} 
            alt={this.props.name} 
            onError={(e)=>{e.target.onerror = null; e.target.src="/favicon.ico"}}
            className="img-fluid" 
          />
        </div>
      </div>

    );
  }
}

export {ImagePopup}