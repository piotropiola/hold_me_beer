import React from 'react';

class Header extends React.Component {

  constructor(props) {
    super(props);
      this.changeColorMode= this.changeColorMode.bind(this);
      this.state = {
        active: false
      };
  }

  componentDidMount(){
    console.log('status',this.props.status)
    this.setState({
      active: this.props.status
    })
  }

  changeColorMode() {
    const currentState = this.state.active;
      this.setState({ 
        active: !currentState 
      });
      if (typeof this.props.action === 'function') {
        this.props.action();
      }
  };

  render() {
    return (
      <div className="container">
        <div className="d-flex header">
          <div>
            <h2 onClick={this.props.toggleSettings} className="header__settings">Settings</h2>
          </div>
          <div className="d-flex toggle_wrap">
            <p>Dark mode</p>
            <div 
              className={"toggle " + (this.state.active ? 'toggle-on': '')} 
              onClick={this.changeColorMode} 
            >
              <div className='toggle-text-off'>OFF</div>
              <div className='glow-comp'></div>
              <div className='toggle-button'></div>
              <div className='toggle-text-on'>ON</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export {Header}