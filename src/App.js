import React from 'react';
import {Header} from './components/header/Header';
import {List} from './components/list/list';
import {apiUrl,setLocalStorage} from './globals/globals';
import './style/main.scss';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoaded:false,
      brewers: [],
      beers: [],
      visible:false,
      settings: {
        'limit': 5,
        'orderBy': 'name',
        'order': 'ASC',
        'darkMode':true
      }
    };

    this.changeMode = this.changeMode.bind(this);
    this.changeSettings = this.changeSettings.bind(this);
    this.toggleSettings = this.toggleSettings.bind(this);

  }


  changeMode() {
      const settings = this.state.settings;
      const currentState = settings.darkMode;
      settings.darkMode = !currentState ;

      this.setState({ 
        settings: settings
      },setLocalStorage('settings',settings));
  }


  toggleSettings(){
    const visible = this.state.visible;
    const currentState = !visible;
    this.setState({
      visible : currentState
    });
  }



  changeSettings(state,value) {
      const settings = this.state.settings;
      if(state == 'limit'){
        settings.limit = value;
      }
      if(state == 'orderBy'){
        settings.orderBy = value;
      }
      if(state == 'order'){
        settings.order = value;
      }
      setLocalStorage('settings',settings);
      this.setState({ 
        settings: settings
      });
  }

  componentDidMount(){
    const get = this.getBrewers();

  }
  componentWillMount(){
    let local = localStorage.getItem('settings');
    if(local){
      this.setState({
        settings:JSON.parse(local)
      })
    }
  }

  getBrewers(){
    let brewers = [];
    let beers = {};
    fetch(apiUrl+"/beers/")
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Something went wrong ...');
        }
      })
      .then((data) => {
        data.map(function(key, index){
          if(brewers.indexOf(key.brewer) === -1){
            brewers.push(key.brewer);
          }

          beers[index] = { 
            'name' : key.name,
            'price': key.price,
            'type': key.type,
            'brewer': key.brewer,
            'thumbnail': key.image_url
          };
        })
        this.setState({
          brewers: brewers,
          beers: beers,
          isLoaded:true
        });
      })
      .catch(error => console.log(error));
  }




  render() {
    const isLoaded = this.state.isLoaded;
    return (
      <div className={"App " + (this.state.settings.darkMode ? 'darkApp': '')}>
        <Header 
          status={this.state.settings.darkMode}
          action={this.changeMode} 
          toggleSettings={this.toggleSettings} 
        />
        {this.state.visible == true &&
          <div className='modalWrap'>
            <div className='modalWrap__container'>
              <div className='modalWrap__container__option'>
                <p>orderBy:</p>
                <div>
                  <span 
                    className={(this.state.settings.orderBy === 'name' ? 'active' : '')}
                    onClick={() => this.changeSettings('orderBy','name')}
                  >Name</span>
                  <span 
                    className={(this.state.settings.orderBy === 'type' ? 'active' : '')}
                    onClick={() => this.changeSettings('orderBy','type')}
                  >Type</span>
                  <span 
                    className={(this.state.settings.orderBy === 'price' ? 'active' : '')}
                    onClick={() => this.changeSettings('orderBy','price')}
                  >Price</span>
                </div>
              </div>
              <div className='modalWrap__container__option'>
                <p>Limit value:</p>
                <div>
                  <span 
                    className={(this.state.settings.limit === 5 ? 'active' : '')}
                    onClick={() => this.changeSettings('limit',5)}
                  >5</span>
                  <span 
                    className={(this.state.settings.limit === 10 ? 'active' : '')}
                    onClick={() => this.changeSettings('limit',10)}
                  >10</span>
                  <span
                    className={(this.state.settings.limit === 15 ? 'active' : '')}
                    onClick={() => this.changeSettings('limit',15)}
                  >15</span>
                </div>
              </div>
              <div className='modalWrap__container__option'>
                <p>order:</p>
                <div>
                  <span 
                    className={(this.state.settings.order === 'ASC' ? 'active' : '')}
                    onClick={() => this.changeSettings('order','ASC')}
                  >ASC</span>
                  <span 
                    className={(this.state.settings.order === 'DESC' ? 'active' : '')}
                    onClick={() => this.changeSettings('order','DESC')}
                  >DESC</span>
                </div>
              </div>
              <a className='close-btn' onClick={this.toggleSettings}>Close</a>
            </div>
          </div>
        }
        {isLoaded ? (
          <div className="container">
            <div className="row lists">

              <List 
                id={1}
                selectOptions={this.state.brewers} 
                beerList={this.state.beers}
                limit={this.state.settings.limit} 
                orderBy={this.state.settings.orderBy}
                order={this.state.settings.order}
              />

              <List 
                id={2}
                selectOptions={this.state.brewers} 
                beerList={this.state.beers}
                limit={this.state.settings.limit} 
                orderBy={this.state.settings.orderBy}
                order={this.state.settings.order}
              />

              <List 
                id={3}
                selectOptions={this.state.brewers} 
                beerList={this.state.beers}
                limit={this.state.settings.limit} 
                orderBy={this.state.settings.orderBy}
                order={this.state.settings.order}
              />

            </div>
            
          </div>
        ) : (
          <h2 className='loading'>Loading</h2>
        )}


      </div>
    );
  }
}
 
export default App;
